EESchema Schematic File Version 4
LIBS:testHardware-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MT41K256M16HA-125_E:MT41K256M16HA-125_E U1
U 1 1 5C975BB7
P 2200 3400
F 0 "U1" H 2200 5767 50  0000 C CNN
F 1 "MT41K256M16HA-125_E" H 2200 5676 50  0000 C CNN
F 2 "MT41K256M16HA-125_E:BGA96C80P6X16_900X1400X120N" H 2200 3400 50  0001 L BNN
F 3 "" H 2200 3400 50  0001 L BNN
F 4 "MT41K256M16HA-125:E" H 2200 3400 50  0001 L BNN "Field4"
F 5 "Micron Technology" H 2200 3400 50  0001 L BNN "Field5"
F 6 "V80a .256mx16 Ddr3 Sdram Plastic Commercial Pbf Fbga 1.35v" H 2200 3400 50  0001 L BNN "Field6"
F 7 "Unavailable" H 2200 3400 50  0001 L BNN "Field7"
F 8 "None" H 2200 3400 50  0001 L BNN "Field8"
	1    2200 3400
	1    0    0    -1  
$EndComp
$Comp
L CPU_NXP_IMX:MCIMX6Y2DVM05AAR U2
U 1 1 5C97FC5B
P 6900 1200
F 0 "U2" H 6950 1365 50  0000 C CNN
F 1 "MCIMX6Y2DVM05AAR" H 6950 1274 50  0000 C CNN
F 2 "BGA289C80P17X17_1400X1400X132:BGA289C80P17X17_1400X1400X132" H 6900 1200 50  0001 C CNN
F 3 "" H 6900 1200 50  0001 C CNN
	1    6900 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2200 3250 2200
Wire Wire Line
	2900 2300 3250 2300
Wire Wire Line
	2900 2400 3250 2400
Wire Wire Line
	2900 2500 3250 2500
Wire Wire Line
	2900 2600 3250 2600
Wire Wire Line
	2900 2700 3250 2700
Wire Wire Line
	2900 2800 3250 2800
Wire Wire Line
	2900 2900 3250 2900
Text Label 3100 2200 0    50   ~ 0
dq0
Text Label 3100 2300 0    50   ~ 0
dq1
Text Label 3100 2400 0    50   ~ 0
dq2
Text Label 3100 2500 0    50   ~ 0
dq3
Text Label 3100 2600 0    50   ~ 0
dq4
Text Label 3100 2700 0    50   ~ 0
dq5
Text Label 3100 2800 0    50   ~ 0
dq6
Text Label 3100 2900 0    50   ~ 0
dq7
Wire Wire Line
	6000 2950 5650 2950
Wire Wire Line
	6000 3050 5650 3050
Wire Wire Line
	6000 3150 5650 3150
Wire Wire Line
	6000 3250 5650 3250
Wire Wire Line
	6000 3350 5650 3350
Wire Wire Line
	6000 3450 5650 3450
Wire Wire Line
	6000 3550 5650 3550
Wire Wire Line
	6000 3650 5650 3650
Text Label 5800 2950 2    50   ~ 0
dq0
Text Label 5800 3050 2    50   ~ 0
dq1
Text Label 5800 3150 2    50   ~ 0
dq2
Text Label 5800 3250 2    50   ~ 0
dq3
Text Label 5800 3350 2    50   ~ 0
dq4
Text Label 5800 3450 2    50   ~ 0
dq5
Text Label 5800 3550 2    50   ~ 0
dq6
Text Label 5800 3650 2    50   ~ 0
dq7
Wire Wire Line
	1500 3100 1150 3100
Wire Wire Line
	1500 3200 1150 3200
Wire Wire Line
	1500 3300 1150 3300
Wire Wire Line
	1500 3400 1150 3400
Wire Wire Line
	1500 3500 1150 3500
Wire Wire Line
	1500 3600 1150 3600
Wire Wire Line
	1500 3700 1150 3700
Wire Wire Line
	1500 3800 1150 3800
Text Label 1200 3100 0    50   ~ 0
a0
Text Label 1200 3200 0    50   ~ 0
a1
Text Label 1200 3300 0    50   ~ 0
a2
Text Label 1200 3400 0    50   ~ 0
a3
Text Label 1200 3500 0    50   ~ 0
a4
Text Label 1200 3600 0    50   ~ 0
a5
Text Label 1200 3700 0    50   ~ 0
a6
Text Label 1200 3800 0    50   ~ 0
a7
Wire Wire Line
	6000 1300 5650 1300
Wire Wire Line
	6000 1400 5650 1400
Wire Wire Line
	6000 1500 5650 1500
Wire Wire Line
	6000 1600 5650 1600
Wire Wire Line
	6000 1700 5650 1700
Wire Wire Line
	6000 1800 5650 1800
Wire Wire Line
	6000 1900 5650 1900
Wire Wire Line
	6000 2000 5650 2000
Text Label 5700 1300 0    50   ~ 0
a0
Text Label 5700 1400 0    50   ~ 0
a1
Text Label 5700 1500 0    50   ~ 0
a2
Text Label 5700 1600 0    50   ~ 0
a3
Text Label 5700 1700 0    50   ~ 0
a4
Text Label 5700 1800 0    50   ~ 0
a5
Text Label 5700 1900 0    50   ~ 0
a6
Text Label 5700 2000 0    50   ~ 0
a7
Wire Wire Line
	1500 1800 1150 1800
Wire Wire Line
	1500 1900 1150 1900
Wire Wire Line
	1500 2000 1150 2000
Text Label 1200 1800 0    50   ~ 0
ck_P
Text Label 1200 1900 0    50   ~ 0
ck_N
Text Label 1200 2000 0    50   ~ 0
cke
Wire Wire Line
	7900 2550 8250 2550
Text Label 8200 2550 2    50   ~ 0
cke
Wire Wire Line
	1500 4700 1150 4700
Wire Wire Line
	1500 4800 1150 4800
Wire Wire Line
	1500 4900 1150 4900
Text Label 1200 4700 0    50   ~ 0
ba0
Text Label 1200 4800 0    50   ~ 0
ba1
Text Label 1200 4900 0    50   ~ 0
ba2
Wire Wire Line
	7900 2200 8250 2200
Wire Wire Line
	7900 2300 8250 2300
Wire Wire Line
	7900 2400 8250 2400
Text Label 8200 2200 2    50   ~ 0
ba0
Text Label 8200 2300 2    50   ~ 0
ba1
Text Label 8200 2400 2    50   ~ 0
ba2
Wire Wire Line
	1500 2200 1150 2200
Wire Wire Line
	1500 2300 1150 2300
Wire Wire Line
	1500 2400 1150 2400
Wire Wire Line
	1500 2500 1150 2500
Wire Wire Line
	1500 2700 1150 2700
Wire Wire Line
	1500 2800 1150 2800
Wire Wire Line
	1500 2900 1150 2900
Text Label 1200 2200 0    50   ~ 0
~cs
Text Label 1200 2300 0    50   ~ 0
dm0
Text Label 1200 2400 0    50   ~ 0
odt
Text Label 1200 2500 0    50   ~ 0
~ras
Text Label 1200 2700 0    50   ~ 0
~we
Text Label 1200 2800 0    50   ~ 0
~reset
Text Label 1200 2900 0    50   ~ 0
dm1
Wire Wire Line
	7900 3800 8250 3800
Text Label 8200 3800 2    50   ~ 0
~reset
Text Label 1200 2600 0    50   ~ 0
~cas
Wire Wire Line
	1500 2600 1150 2600
Wire Wire Line
	7900 1550 8250 1550
Text Label 8200 1550 2    50   ~ 0
~cas
Wire Wire Line
	7900 3500 8250 3500
Text Label 8200 3500 2    50   ~ 0
~ras
Wire Wire Line
	7900 3600 8250 3600
Text Label 8200 3600 2    50   ~ 0
~we
Wire Wire Line
	7900 1350 8250 1350
Text Label 8200 1350 2    50   ~ 0
~cs
Wire Wire Line
	7900 1950 8250 1950
Text Label 8200 1950 2    50   ~ 0
odt
Wire Wire Line
	7900 1800 8250 1800
Text Label 8200 1800 2    50   ~ 0
dm1
Wire Wire Line
	7900 1700 8250 1700
Text Label 8200 1700 2    50   ~ 0
dm0
Wire Wire Line
	2900 4400 3300 4400
Wire Wire Line
	2900 4500 3300 4500
Wire Wire Line
	2900 4200 3300 4200
Wire Wire Line
	2900 4100 3300 4100
Text Label 3100 4400 0    50   ~ 0
ldqs_P
Text Label 3100 4500 0    50   ~ 0
ldqs_N
Text Label 3100 4100 0    50   ~ 0
udqs_P
Text Label 3100 4200 0    50   ~ 0
udqs_N
Wire Wire Line
	1500 3900 1150 3900
Wire Wire Line
	1500 4000 1150 4000
Wire Wire Line
	1500 4100 1150 4100
Wire Wire Line
	1500 4200 1150 4200
Wire Wire Line
	1500 4300 1150 4300
Wire Wire Line
	1500 4400 1150 4400
Wire Wire Line
	1500 4500 1150 4500
Wire Wire Line
	2900 3000 3250 3000
Wire Wire Line
	2900 3100 3250 3100
Wire Wire Line
	2900 3200 3250 3200
Wire Wire Line
	2900 3300 3250 3300
Wire Wire Line
	2900 3400 3250 3400
Wire Wire Line
	2900 3500 3250 3500
Wire Wire Line
	2900 3600 3250 3600
Wire Wire Line
	2900 3700 3250 3700
Text Label 3100 3000 0    50   ~ 0
dq8
Text Label 3100 3100 0    50   ~ 0
dq9
Text Label 3100 3200 0    50   ~ 0
dq10
Text Label 3100 3300 0    50   ~ 0
dq11
Text Label 3100 3400 0    50   ~ 0
dq12
Text Label 3100 3500 0    50   ~ 0
dq13
Text Label 3100 3600 0    50   ~ 0
dq14
Text Label 3100 3700 0    50   ~ 0
dq15
Text Label 1200 3900 0    50   ~ 0
a8
Text Label 1200 4000 0    50   ~ 0
a9
Text Label 1200 4100 0    50   ~ 0
a10
Text Label 1200 4200 0    50   ~ 0
a11
Text Label 1200 4300 0    50   ~ 0
a12
Text Label 1200 4400 0    50   ~ 0
a13
Text Label 1200 4500 0    50   ~ 0
a14
Wire Wire Line
	6000 2100 5650 2100
Wire Wire Line
	6000 2200 5650 2200
Wire Wire Line
	6000 2300 5650 2300
Wire Wire Line
	6000 2400 5650 2400
Wire Wire Line
	6000 2500 5650 2500
Wire Wire Line
	6000 2600 5650 2600
Wire Wire Line
	6000 2700 5650 2700
Text Label 5700 2100 0    50   ~ 0
a8
Text Label 5700 2200 0    50   ~ 0
a9
Text Label 5700 2300 0    50   ~ 0
a10
Text Label 5700 2400 0    50   ~ 0
a11
Text Label 5700 2500 0    50   ~ 0
a12
Text Label 5700 2600 0    50   ~ 0
a13
Text Label 5700 2700 0    50   ~ 0
a14
Wire Wire Line
	6000 3750 5650 3750
Wire Wire Line
	6000 3850 5650 3850
Wire Wire Line
	6000 3950 5650 3950
Wire Wire Line
	6000 4050 5650 4050
Wire Wire Line
	6000 4150 5650 4150
Wire Wire Line
	6000 4250 5650 4250
Wire Wire Line
	6000 4350 5650 4350
Wire Wire Line
	6000 4450 5650 4450
Text Label 5800 3750 2    50   ~ 0
dq8
Text Label 5800 3850 2    50   ~ 0
dq9
Text Label 5800 3950 2    50   ~ 0
dq10
Text Label 5800 4050 2    50   ~ 0
dq11
Text Label 5800 4150 2    50   ~ 0
dq12
Text Label 5800 4250 2    50   ~ 0
dq13
Text Label 5800 4350 2    50   ~ 0
dq14
Text Label 5800 4450 2    50   ~ 0
dq15
Wire Wire Line
	7900 2800 8250 2800
Wire Wire Line
	7900 2900 8250 2900
Text Label 8200 2800 2    50   ~ 0
ck_P
Text Label 8200 2900 2    50   ~ 0
ck_N
Wire Wire Line
	7900 3350 8300 3350
Wire Wire Line
	7900 3250 8300 3250
Text Label 8100 3250 0    50   ~ 0
udqs_P
Text Label 8100 3350 0    50   ~ 0
udqs_N
Wire Wire Line
	7900 3050 8300 3050
Wire Wire Line
	7900 3150 8300 3150
Text Label 8100 3050 0    50   ~ 0
ldqs_P
Text Label 8100 3150 0    50   ~ 0
ldqs_N
$EndSCHEMATC
