PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
BGA96C80P6X16_900X1400X120N
$EndINDEX
$MODULE BGA96C80P6X16_900X1400X120N
Po 0 0 0 15 00000000 00000000 ~~
Li BGA96C80P6X16_900X1400X120N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -1.96582 -8.66755 1.00295 1.00295 0 0.05 N V 21 "BGA96C80P6X16_900X1400X120N"
T1 -1.32991 8.65623 1.00369 1.00369 0 0.05 N V 21 "VAL**"
DS -4.55 -7.05 4.55 -7.05 0.127 21
DS 4.55 -7.05 4.55 7.05 0.127 21
DS 4.55 7.05 -4.55 7.05 0.127 21
DS -4.55 7.05 -4.55 -7.05 0.127 21
DS -5.05 -7.55 5.05 -7.55 0.05 26
DS 5.05 -7.55 5.05 7.55 0.05 26
DS 5.05 7.55 -5.05 7.55 0.05 26
DS -5.05 7.55 -5.05 -7.55 0.05 26
DS -4.55 -7.05 4.55 -7.05 0.127 27
DS -4.55 7.05 -4.55 -7.05 0.127 27
DS 4.55 -7.05 4.55 7.05 0.127 27
DS 4.55 7.05 -4.55 7.05 0.127 27
DC -5 -6 -4.9 -6 0.2 21
DC -3.2 -6 -3.1 -6 0.2 27
$PAD
Sh "A1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -6
$EndPAD
$PAD
Sh "A2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -6
$EndPAD
$PAD
Sh "A3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 -6
$EndPAD
$PAD
Sh "A7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 -6
$EndPAD
$PAD
Sh "A8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -6
$EndPAD
$PAD
Sh "A9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -6
$EndPAD
$PAD
Sh "B1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -5.2
$EndPAD
$PAD
Sh "B2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -5.2
$EndPAD
$PAD
Sh "B3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 -5.2
$EndPAD
$PAD
Sh "B7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 -5.2
$EndPAD
$PAD
Sh "B8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -5.2
$EndPAD
$PAD
Sh "B9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -5.2
$EndPAD
$PAD
Sh "C1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -4.4
$EndPAD
$PAD
Sh "C2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -4.4
$EndPAD
$PAD
Sh "C3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 -4.4
$EndPAD
$PAD
Sh "C7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 -4.4
$EndPAD
$PAD
Sh "C8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -4.4
$EndPAD
$PAD
Sh "C9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -4.4
$EndPAD
$PAD
Sh "D1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -3.6
$EndPAD
$PAD
Sh "D2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -3.6
$EndPAD
$PAD
Sh "D3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 -3.6
$EndPAD
$PAD
Sh "D7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 -3.6
$EndPAD
$PAD
Sh "D8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -3.6
$EndPAD
$PAD
Sh "D9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -3.6
$EndPAD
$PAD
Sh "E1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -2.8
$EndPAD
$PAD
Sh "E2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -2.8
$EndPAD
$PAD
Sh "E3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 -2.8
$EndPAD
$PAD
Sh "E7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 -2.8
$EndPAD
$PAD
Sh "E8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -2.8
$EndPAD
$PAD
Sh "E9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -2.8
$EndPAD
$PAD
Sh "F1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -2
$EndPAD
$PAD
Sh "F2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -2
$EndPAD
$PAD
Sh "F3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 -2
$EndPAD
$PAD
Sh "F7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 -2
$EndPAD
$PAD
Sh "F8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -2
$EndPAD
$PAD
Sh "F9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -2
$EndPAD
$PAD
Sh "G1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -1.2
$EndPAD
$PAD
Sh "G2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -1.2
$EndPAD
$PAD
Sh "G3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 -1.2
$EndPAD
$PAD
Sh "G7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 -1.2
$EndPAD
$PAD
Sh "G8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -1.2
$EndPAD
$PAD
Sh "G9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -1.2
$EndPAD
$PAD
Sh "H1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -0.4
$EndPAD
$PAD
Sh "H2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -0.4
$EndPAD
$PAD
Sh "H3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 -0.4
$EndPAD
$PAD
Sh "H7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 -0.4
$EndPAD
$PAD
Sh "H8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -0.4
$EndPAD
$PAD
Sh "H9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -0.4
$EndPAD
$PAD
Sh "J1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 0.4
$EndPAD
$PAD
Sh "J2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 0.4
$EndPAD
$PAD
Sh "J3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 0.4
$EndPAD
$PAD
Sh "J7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 0.4
$EndPAD
$PAD
Sh "J8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 0.4
$EndPAD
$PAD
Sh "J9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 0.4
$EndPAD
$PAD
Sh "K1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 1.2
$EndPAD
$PAD
Sh "K2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 1.2
$EndPAD
$PAD
Sh "K3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 1.2
$EndPAD
$PAD
Sh "K7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 1.2
$EndPAD
$PAD
Sh "K8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 1.2
$EndPAD
$PAD
Sh "K9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 1.2
$EndPAD
$PAD
Sh "L1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 2
$EndPAD
$PAD
Sh "L2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 2
$EndPAD
$PAD
Sh "L3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 2
$EndPAD
$PAD
Sh "L7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 2
$EndPAD
$PAD
Sh "L8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 2
$EndPAD
$PAD
Sh "L9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 2
$EndPAD
$PAD
Sh "M1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 2.8
$EndPAD
$PAD
Sh "M2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 2.8
$EndPAD
$PAD
Sh "M3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 2.8
$EndPAD
$PAD
Sh "M7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 2.8
$EndPAD
$PAD
Sh "M8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 2.8
$EndPAD
$PAD
Sh "M9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 2.8
$EndPAD
$PAD
Sh "N1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 3.6
$EndPAD
$PAD
Sh "N2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 3.6
$EndPAD
$PAD
Sh "N3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 3.6
$EndPAD
$PAD
Sh "N7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 3.6
$EndPAD
$PAD
Sh "N8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 3.6
$EndPAD
$PAD
Sh "N9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 3.6
$EndPAD
$PAD
Sh "P1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 4.4
$EndPAD
$PAD
Sh "P2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 4.4
$EndPAD
$PAD
Sh "P3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 4.4
$EndPAD
$PAD
Sh "P7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 4.4
$EndPAD
$PAD
Sh "P8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 4.4
$EndPAD
$PAD
Sh "P9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 4.4
$EndPAD
$PAD
Sh "R1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 5.2
$EndPAD
$PAD
Sh "R2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 5.2
$EndPAD
$PAD
Sh "R3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 5.2
$EndPAD
$PAD
Sh "R7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 5.2
$EndPAD
$PAD
Sh "R8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 5.2
$EndPAD
$PAD
Sh "R9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 5.2
$EndPAD
$PAD
Sh "T1" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 6
$EndPAD
$PAD
Sh "T2" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 6
$EndPAD
$PAD
Sh "T3" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.6 6
$EndPAD
$PAD
Sh "T7" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.6 6
$EndPAD
$PAD
Sh "T8" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 6
$EndPAD
$PAD
Sh "T9" C 0.41 0.41 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 6
$EndPAD
$EndMODULE BGA96C80P6X16_900X1400X120N
